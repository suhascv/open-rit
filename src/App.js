//styling
import './App.scss';


//components
import Navigation from './components/navigation.js';
import Home from './components/home/home.js';
import OpenCalendar from'./components/calendar/openCalendar.js';
import People from './components/people/people.js';
import Footer from './components/footer.js';
import Page from './components/pages/page.js';


//jsx array
import {StudentsSections} from './components/students/students.js';
import {ResearchSections} from './components/research/research.js';
import {FacultySections} from './components/faculty/faculty.js';
import {AboutSections}  from './components/about/about.js';

//data
import navigations from './data/navagations.json';
import routes from './data/routes.json';


//libraries
import {
  Switch,
  Route
} from "react-router-dom";



var path = process.env.PUBLIC_URL;

function App() {
  
  
  return (
    <div className="App">
      
      <Navigation navigations={navigations} logo={path+"/images/general/logo.jpg"}/>
      <main>
      <Switch>
        <Route exact path={"/"} component={Home}/>
        <Route exact path={"/students"}>
          <Page pageTitle="Students" 
                coverImage={path+"/images/general/studentCover.jpg"} 
                sections={routes["students"] } 
                ribbon={path+"/images/general/ribbon.svg"}
                sectionElements = {StudentsSections}
                 />
        </Route>
        <Route exact path={"/faculty"}>
          <Page pageTitle="Faculty and Staff" 
                coverImage={path+"/images/general/studentCover.jpg"} 
                sections={routes["faculty"]} 
                ribbon={path+"/images/general/ribbon.svg"}
                sectionElements={FacultySections}
                />
        </Route>
        <Route exact path={"/research"}>
          <Page pageTitle="Research" 
                coverImage={path+"/images/general/studentCover.jpg"} 
                sections={routes["research"]} 
                ribbon={path+"/images/general/ribbon.svg"}
                sectionElements={ResearchSections}
                />
        </Route>
        <Route exact path={"/about"}>
          <Page pageTitle="About" 
                coverImage={path+"/images/general/studentCover.jpg"} 
                sections={routes["about"]} 
                ribbon={path+"/images/general/ribbon.svg"}
                sectionElements={AboutSections}
                />
        </Route>
        <Route exact path="/calendar">
        <OpenCalendar/>
        </Route>
        <Route exact path="/people"><People/></Route>
      </Switch>
      <Footer/>
      </main>
      
    </div>
  );
}

export default App;
