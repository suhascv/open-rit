import './About.scss';
import CardSlider from '../cardSlider.js';
import advisoryBoardCards from './advisoryBoard.json';
import CardGrid from '../cardGrid.js';

const path= process.env.PUBLIC_URL;
const libreCoOpStudents = [

    {
        image:path+"/images/about/qfoster.jpg",
        primary:"Quinn Foster",
        secondary:[],
        description:["Writer"]
    },
    {
        image:path+"/images/about/juliaHawley.jpg",
        primary:"Julia Hawley",
        secondary:[],
        description:["UX Designer"]
    },
    {
        image:path+"/images/about/rahul.jpg",
        primary:"Rahul Jaiswal",
        secondary:[],
        description:["User Experience Designer"]
    },
    {
        image:path+"/images/about/shanee.png",
        primary:"Shanée Gordon",
        secondary:[],
        description:["Writer"]
    },
    {
        image:path+"/images/about/suhas.png",
        primary:"Suhas CV",
        secondary:[],
        description:["Full Stack Developer"]
    }

]







const WhatWeDo = () =>(
    <div className="what-we-do">
    
        <div className="quote">
                “The aim is to help RIT projects and research succeed even more — 
                    and one of the best ways to do that is by using open work and the open community.” - <b>Stephen Jacobs, director of Open@RIT</b>
        </div>
            
        <p>
        As the Open Programs Office for RIT we’re to support all members
         of the RIT community in their Open Work, whether it be open data, 
         open hardware, open-source software, open educational resources.
        </p>
        <div className="sub-sections">
            <h2 >
                We Provide
            </h2>
            <p>
            <a target="_blank" href="https://calendly.com/open-rit-org/">Office hours</a> for questions around Open Work<br/><br/>

            <a>Assitance</a> in writing portions of your proposals on Open licensing, distribution and sustainability<br/><br/>

            <a>LibreCorps</a> talent to develop and deliver on your proposals for new work andcurrent efforts you wish to expand on.<br/><br/>

            <a>The Open@RIT Fellows program</a>, CFPs for the second round will open fall semester of 2021<br/><br/>

            <a>Curated resource collections</a> (coming soon) <br/><br/>

            <a>Professional development programming</a> (Starting Fall of 2021)<br/><br/>
            </p>
        </div>
        <div className="sub-sections">
            <h2 >
            Project Development and Management
            </h2>
            <p> Want to begin contributing to, or creating your own, Open Work? <br/>
                Want to bring your previously proprietary project in to the Open? <br/>
                We can help you on tools and infrastructure, managing Open communities, licensing and  best practices.</p>
        </div>
        <div className="sub-sections">
            <h2 >
            Research Support
            </h2>
            <p> <span>Grant Proposals:</span>  Does your grant require that your outcomes and/or projects be released as 	Open?
                We can help you to write the sections on licensing and disseminating/distributing the 
                work and on the sustainability plan for it.</p>
            <p>
            <span>LibreCorps:</span> Our team of Open Work experts can provide a wide range of skills and
            	support to your Funded digital project or the proposal you’re writing for it.
            </p>
            <p><span>Open@RIT Fellows:</span>  The Alfred P. Sloan Foundation has provided funding for a		
                LibreCorps team to support the Open Work of RIT faculty and staff who wish to contribute
                to existing Open work projects or build a community of contributors for their own projects.	
                We send out a call for proposals a few times / year to the campus wide list and to our 	Open@RIT list.
                <a> You can join that list here.</a> 
            </p>

        </div>

    </div>
);

const History =() =>(
    <div className="history">
        <p>
        Everyone needs support and that’s what we are here to do! We help by contributing a standardized set of documentation and resources
        to help make the process of open sourcing your project as easy as possible. Documentation templates, licensing recommendations, 
        and best practices and how-tos are just a few of the ways we help provide hands on community infrastructure building through our
         LibreCorps services. We provide in-person consultation (ask us anything!) and tools and infrastructure to help make managing 
         larger communities easier (IEEE SA Open).
         Whether it be open data, open hardware, open-source software, open education resources, or any other Open work,
        we are here to help create or strengthen a community of practice around your research project. Or in other words,
        that of Stephen Jacobs, director of Open@Rit.
        </p>
    </div>
)



const HowWork = () =>(
    <div>
        <p>
        Everyone needs support and that’s what we are here to do! We help by contributing a standardized set of documentation and resources
         to help make the process of open sourcing your project as easy as possible. Documentation templates, licensing recommendations,
        and best practices and how-tos are just a few of the ways we help provide hands on community infrastructure building through our
         LibreCorps services. We provide in-person consultation (ask us anything!) and tools and infrastructure to help make managing larger
        communities easier (IEEE SA Open).<br/>
        Whether it be open data, open hardware, open-source software, open education resources, or any other Open work, we are here to help create or strengthen a community of practice around your research project. Or in other words, that of Stephen Jacobs, director of Open@Rit,
        </p>
        <p className="quote">
        “The aim is to help RIT projects and research succeed even more — and
            one of the best ways to do that is by using open work and the open community.”
        </p>
        <p>
        We provide a digital service for the non-digital faculty, so if it’s your first time in open work,
        you’re looking to a create a new open work, or want to move a previously closed IP to open, Open@RIT is here to lend a hand.
        </p>
    </div>
);

const AboutPeople = () =>(
    <div className="people">
        <div className="people-1">
            <h2 className="people-1-role">Director</h2>
            <div className="people-1-img">
                <img src={path+"/images/about/director.jpeg"}></img>
            </div>
            <div className="people-1-info-mobile">
            <h2>Stephen Jacobs</h2>
                    <h4>Professor</h4>
                    <h4>School of Interactive Games and Media</h4>
                    <h4>GCCIS</h4>
                    <a>sj@magic.rit.edu</a>
            </div>
            <div className="people-1-info">
                    <h2>Stephen Jacobs</h2>
                    <h4>Professor</h4>
                    <h4>School of Interactive Games and Media</h4>
                    <h4>GCCIS</h4>
                    <br/>
                    <a>sj@magic.rit.edu</a>
            </div>
            <div className="people-1-description">
                
                <p>Stephen Jacobs is a professor with the School of Interactive Games and Media and an interdisciplinary scholar 
                    who works in several different areas that often overlap including Free and Open Source Software and Free Culture.
                    Digital Humanities, Game Design and History and Interactive Narrative. He also holds the position of Visiting Scholar
                    at The Strong National Museum of Play.  His Open Work has been funded by The Ford Foundation, UNICEF Innovation, 
                    Red Hat Inc, Northern Telecom and AT&T. Professor Jacobs received the Provost’s Award for Excellence in Faculty
                     Mentoring for 2019-2020.
                </p>
            </div>

        </div>
        <div className="people-1">
            <h2 className="people-1-role">Assistant Director</h2>
            <div className="people-1-img">
                <img src={path+"/images/about/assistantDirector.png"}></img>
            </div>
            <div className="people-1-info-mobile">
                    <h2>Michael Nolan</h2>
                    <h4>Staff</h4>
                    <h4>Open@RIT</h4>
                    <h4>Research</h4>
                    <a>mpnopen@rit.edu</a>
            </div>
            <div className="people-1-info">
                    <h2>Michael Nolan</h2>
                    <h4>Staff</h4>
                    <h4>Open@RIT</h4>
                    <h4>Research</h4>
                    <br/>
                    <a>mpnopen@rit.edu</a>
            </div>
            <div className="people-1-description">
                
                <p>Mike Nolan serves as the Assistant Director of Open@RIT.
                  A former RIT alum, he went on to work in the tech industry at companies such as GIPHY and Mozilla.
                   He then proceeded to work in the humanitarian aid sector abroad (Jordan & UK) f
                   or organizations such as the International Rescue Committee and UNICEF. 
                   He continues to pursue his mission of technology and social good by helping lead RIT's open programs center.
                </p>
            </div>
        </div>
        <div className="people-2">
            <h1 className="people-2-role">Fellow Support Team</h1>
            <CardSlider Identity="slider-1" 
                    cards={libreCoOpStudents} 
                    cardMargin={16} 
                    primaryFont={14}
                    card
                    sliderHeight={300}
                    infoHeight={10}
                    infoBackground={"#ffffff"}
                    displayNumber={3}
                    />
        </div>
    </div>
);
/*

const AdvisoryBoard = () =>(
    <div className="advisory-board">
        {advisoryBoardCards.map((member,index)=>(
            <div className="advisory-member" key={index}>
                <h3>{member.primary}</h3>
                {member.secondary.map((sec,ix)=>(<p key={ix}>{sec}</p>))}
            </div>
        ))}
    </div>
);


*/
const AdvisoryBoard=()=>(
        <CardGrid cards={advisoryBoardCards} 
                  Columns={4} 
                  ImageHeight={100}
                  ImageWidth={50}
                  />
    
)

const OrganiztionsWeWork=()=>(
    
        <div className="organizations-we-work">
            <a target="_blank" href="https://www.linuxfoundation.org/">Linux Foundation</a>

            <a target="_blank" href="https://chaoss.community/">CHAOSS Value Working Group</a>

            <a target="_blank" href="https://standards.ieee.org/">IEEE SA Open Working Group</a>

            <a target="_blank" >Sustain University Education Working Group</a>

            <a target="_blank" >OSPO++</a>

            <a target="_blank" >ToDo</a>
        </div>
    
)
const OrganiztionsWork = () =>(
    <div className="organizations-work">
        <div>
            <img src={path+"/images/about/linuxFoundation.png"}></img>
            <div className="organizations-work-info">
                <a>Linux Foundation</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                     Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
                </div>
        </div>
        <div>
            <img src={path+"/images/about/chaoss.png"}></img>
            <div className="organizations-work-info">
                <a>CHAOSS Value WG</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                </p>
            </div>
        </div>
        <div>
            <img src={path+"/images/about/ieeeSa.png"}></img>
            <div className="organizations-work-info">
            <a>IEEE SA Open WG</a>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                     Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
            </div>
        </div>
        <div>
            <img src={"https://claws.rit.edu/photos/images/placeholder.jpg"}></img>
            <div className="organizations-work-info">
                <a>Sustain Univ Ed WG</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                     Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
            </div>
        </div>
        <div>
            <img src="https://claws.rit.edu/photos/images/placeholder.jpg"></img>
            <div className="organizations-work-info"><a>OSPO++</a>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                     Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
            </div>
        </div>
        
        <div>
            <img src="https://claws.rit.edu/photos/images/placeholder.jpg"></img>
            <div className="organizations-work-info"><a>ToDo</a>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
            </div>
        </div>
        
    </div>
);
export const AboutSections=[WhatWeDo,History,AboutPeople,AdvisoryBoard,OrganiztionsWeWork]