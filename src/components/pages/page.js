import React from 'react';
import './Page.scss';
import * as Icon from 'react-feather';

const path = process.env.PUBLIC_URL;
const isMobile=()=>{
    return window.innerWidth<=750?true:false;
}

export default class Page extends React.Component {

    constructor(props){
        super(props);
        var temp =[]
        for(let i=0;i<this.props.sections.length;i++){
            temp.push("");
        }
        this.state={
            id:this.props.pageTitle.replaceAll(' ','-'),
            numberOfSections:this.props.sections.length,
            selected:-1,
            hovered:-1,
            jumpSection:false,
            path:"#"+this.props.pageTitle.replaceAll(' ','-')+"-",
            jumped:false,
            defaultAnimation:temp,
            sectionAnimation:temp
        }
        this.handleScroll = this.handleScroll.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleHover = this.handleHover.bind(this);
        this.handleJump = this.handleJump.bind(this);
        this.jumpTo=this.jumpTo.bind(this);
    }

    handleJump(){
        this.setState({jumpSection:this.state.jumpSection?false:true});
    }

    jumpTo(){
        setTimeout(()=>{
            if(isMobile()){
            window.scrollBy(0,-120);
            }
            else{
                window.scrollBy(0,-70);   
            }
            
        },500);
        
    }

    handleScroll(){
        let sidenav = document.getElementById(this.props.pageTitle+"-sidenav");
        let tmp= this.state.defaultAnimation;
        if(window.scrollY>420){
            sidenav.style.top = "100px";
            sidenav.style.position="fixed";
            
        }
        else{
            sidenav.style.position="absolute";
            sidenav.style.top = "530px";
        }
        var scrollMaxY = window.scrollMaxY || (document.documentElement.scrollHeight - document.documentElement.clientHeight)
        if(scrollMaxY- window.scrollY<100){
            sidenav.style.top="-100px"
        }
        var cnt=-1;
        try{
        for(let i=0;i<this.state.numberOfSections;i++){
            let ele= document.getElementById(this.props.pageTitle.replaceAll(' ','-')+"-"+i);
            let posY=ele.getBoundingClientRect().top;
            if(posY<window.innerHeight-200){
            cnt++;
            }
            else{
                break;
            }
            
            
            }
        
            

        let progressBar = document.getElementById('progress-bar');
        progressBar.style.height=cnt*40+"px";
        }
        catch(e){
            console.log(e);
        }
        if(isMobile()){
            let ele =document.getElementById('jump-menu')
            if(window.scrollY>70){
            ele.style.display='flex';
            }
            else{
                ele.style.display='none';
                this.setState({jumpSection:false});
            }
        }

    }

    handleClick(index){
        this.setState({selected:index});
        
    }
    handleHover(index){
        if(index!==null){
        this.setState({hovered:index});
        }
        else{
            this.setState({hovered:-1});
        }
    }
    componentDidMount(){
        window.addEventListener('scroll',this.handleScroll);
        console.log(this.props);
        this.jumpTo();
        
        
        
    }
    

    render(){
        return(
            <div className="page" id={this.state.id} onScroll={this.handleScroll}>
                <div id="jump-menu" className="page-jump-to-section">
                    <div className="jump-header" onClick={this.handleJump}>
                        <h3>Jump to Section</h3>
                        {this.state.jumpSection?<Icon.ChevronUp />:<Icon.ChevronDown/>}
                        
                        </div>
                    {this.state.jumpSection?<div id="jump-to-section">
                        {this.props.sections.map((section,index)=>
                            (<a key={index} 
                                onClick={()=>{
                                    this.jumpTo()
                                    this.handleJump()
                                }}
                                href={this.state.path+index} >{section}</a>))}
                    </div>:null}
                    
                </div>
                <img className="page-cover" src={this.props.coverImage} alt="cover"></img>
                <div className="cover-title">
                    <h1 >{this.props.pageTitle}</h1>
                </div>
                {this.props.ribbon?
                <img className="cover-background" src={this.props.ribbon} alt="rit"></img>:
                null
                }
                    
                    <div className="page-sidenav" id={this.props.pageTitle+"-sidenav"}>
                        <h3 className="page-title">{"Open@RIT"}<span>{"/"+this.props.pageTitle}</span></h3>
                        <div id="progress-bar" className="progress-bar"></div>
                        <div className="page-menu">
                        {this.props.sections.map((section,index)=>(
                                <a key={index} href={this.state.path+index} 

                                onMouseEnter={()=>this.handleHover(index)}
                                onMouseLeave={()=>this.handleHover(null)}
                                onClick={()=>{this.handleClick(index)
                                             this.jumpTo()}}
                                
                                style={{color:(this.state.selected===index||this.state.hovered===index)?"black":"#727578"}}

                                >
                                {section}
                                </a>
                            ))}
                        </div>
                        
                    </div>
                    <div className="page-sections">
                        {this.props.landingContent?
                        <div className="page-section">
                            {this.props.landingContent()}
                        </div>:null
                        }
                        {this.props.sections.map((section,index)=>(
                            <div className={(index===0?"page-section section-first":"page-section")} 
                                 key={index} 
                                 id ={this.props.pageTitle.replaceAll(' ','-')+"-"+index}>
                               <h1>{section}</h1> 
                               
                               {this.props.sectionElements?this.props.sectionElements[index]():null}
                            </div>
                        ))}
                    </div>
            </div>
        )
    }
}