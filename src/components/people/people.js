import './People.scss';

import director from './director.json'; 
import assistantDirector from "./assistantDirector.json";
import { useEffect, useState } from 'react';


const getPath =()=>{
    console.log(window.location.href.split("#")[1]);
    return window.location.href.split("#")[1];
}


const defaultColor={
    director:"black",
    assistantDirector:"black"
}
export default function People(){

    const [target,setTarget] = useState(defaultColor);
    useEffect(()=>{
        var newTarget = {...defaultColor};
        newTarget[getPath]="red";
        setTarget(newTarget);
        console.log(target);
    })
    return(
        <div className="container">
            <div className="people">
                <header>
                    <h2>Open@RIT Profiles</h2>
                    <div className="web-only-profiles" >
                        <a href="#director" style={{color:target['director']}} >Director</a>
                        <a href="#assistant-director" style={{color:target["assistant-director"]}}>AssistantDirector</a>
                        <a>Students</a>
                        <a>Alum</a>
                    </div>
                    </header>
                <div className="profile-section" id="director">
                    <h2>Director</h2>
                    <div className="section-container">
                    <DisplayPeople data={director}/>
                    </div>
                </div>
                <div className="profile-section" id="assistant-director">
                    <h2>Assistant Director</h2>
                    <div className="section-container">
                    <DisplayPeople data={assistantDirector}/>
                    </div>
                </div>
            </div>

        </div>
    )
}

const DisplayPeople = (props)=>{
    return(
        <div className="display-people">
            {props.data.url?<h3><a href={props.data.url}>{props.data.name}</a></h3>:<h3>{props.data.name}</h3>}
            {props.data.tags.map((tag,index)=>(
                <h3 key="index">{tag}</h3>
            ))}
        </div>
    )
}