import './Home.scss';
import Intro from './intro.js';
//import ICalendarLink from "react-icalendar-link";
import CardSlider from '../cardSlider.js';
//import CardGrid from '../cardGrid.js';
var path = process.env.PUBLIC_URL;




const loeremIspum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."

const projectCards=[
    {
        image:path+"/images/fellows/StevenYi.jpeg",
        primary:"Steven Yi",
        secondary:["Assistant Professor"],
        description:["School of Interactive Games and Media"],
        url:"https://www.rit.edu/directory/syyigm-steven-yi"
    },
    {
        image:path+"/images/fellows/WMHarris.jpeg",
        primary:"W Michelle Harris",
        secondary:["Associate Professor"],
        description:["School of Interactive Games and Media"],
        url:"https://www.rit.edu/directory/wmhics-w-michelle-harris"
    },
    {
        image:path+"/images/fellows/ChrisKurz.jpeg",
        primary:"Christopher Kurz",
        secondary:["Professor"],
        description:["National Technical Institute for the Deaf"],
        url:"https://www.rit.edu/directory/caknsp-christopher-kurz"
    },
    {
    image:path+"/images/fellows/JScottHawker.jpeg",
    primary:"J Scott Hawker",
    secondary:["Associate Professor"],
    description:["Department of Software Engineering"],
    url:"https://www.rit.edu/directory/jshvse-j-scott-hawker"
    }
]



export default function Home(){
    

    return(
        <div className="home">
            <Intro/>
            <div className="home-container">
                <div className="container"> 
                        <div className="content">
                            <div className="mobile-intro">
                                <h2>
                                    Open@RIT is a Key Research Center of the University and serves as the Open Programs Office of the University.
                                    It is part of the Office of the Vice President of Research
                                </h2>
                            </div>
                            <div className='home-intro'>
                                <h3>
                                Open@RIT is dedicated to fostering the collaborative engine of Open across the University for Faculty, 
                                Staff and Students. Its goals are to discover, and grow, the footprint of RIT’s impact on all things Open
                                including, but not limited to, Open Source Software, Open Data, Open Hardware, Open Educational Resources 
                                and Creative Commons licensed efforts, what we like to refer to in aggregate as Open Work. 
                                </h3>
                            </div>
                            
                            
                            <h1>Open@RIT Fellows</h1>
                            <CardSlider Identity="slider-1" 
                                cards={projectCards} 
                                cardMargin={16} 
                                primaryFont={14}
                                sliderBackground={"#f8f9fa"}
                                sliderHeight={400}
                                infoHeight={40}
                                primaryColor={"#F76902"}
                                infoBackground={"#f8f9fa"}
                                displayNumber={4}
                                />
                            
                            
                            <h1>News and Events</h1>
                            <div className="home-news-and-events">
                                <div className="news">
                                    <div className="home-news-item">
                                        <a target="_blank" href="https://www.rit.edu/news/rit-ntids-world-around-you-platform-chosen-inclusive-education-award">
                                            <h3>RIT/NTID’s World Around You’ platform chosen for Inclusive Education award</h3>
                                        </a>
                                        <p>World Around You, a multilingual platform created by a team at NTID, was
                                            selected as the winner of the Inclusive Education award by the
                                            mEducation Alliance. The goal of World Around You, or WAY, is to
                                            increase global literacy for deaf children by providing greater
                                            access to sign languages, early-grade reading materials, vocabulary,
                                            reading instruction, and digital games.
                                        </p>
                                    </div>
                                    <div className="home-news-item">
                                        <a target="_blank" href="https://www.scientificamerican.com/article/we-must-tear-down-the-barriers-that-impede-scientific-progress/">
                                        <h3>We Must Tear Down the Barriers That Impede Scientific Progress</h3>
                                        </a>
                                        <p>Scientific American highlights Open@RIT, RIT's open programs office, 
                                            as a notable example of embracing “open science.”
                                        </p>
                                    </div>
                                    <div className="home-news-item">
                                        <a target="_blank" href="https://www.rit.edu/news/openrit-receives-sloan-foundation-grant-support-open-work-across-university">
                                        <h3>Open@RIT receives Sloan Foundation grant to support open work across the university</h3>
                                        </a>
                                        <p>RIT’s open programs office has received a nearly $500,000 grant from the
                                            Alfred P. Sloan Foundation to measure and strengthen support of the
                                            faculty and staff who do work in the open community, including open
                                            source software, open data, open hardware, open educational
                                            resources, Creative Commons licensed work, open research, and other
                                            open work”
                                        </p>
                                    </div>
                                </div>
                                <div className="podcasts">
                                    <div className="home-podcasts-item">
                                    <a target="_blank" href="https://foss-backstage.de/session/culture-open-source-between-institutions">
                                                <h3>Panel discussion about OSPO</h3>
                                    </a>
                                            <p>
                                                Learn more about the Culture of Open Source between Institutions with Cat Allman (Google), 
                                                Stephen Jacobs (Open@RIT), Jonathan Fink (Portland State University) and Hong Phuc
                                                Dang (FOSSASIA, OSI), Josh Simmons (OSI) and Richard Littauer (Mosslabs)
                                            </p>
                                    </div>
                                    <div className="home-podcasts-item">
                                    <a target="_blank" href="https://podcast.chaoss.community/22">
                                                <h3>The Culture of Open Source between Institutions : CHAOSS</h3>
                                    </a>
                                            <p>
                                            February 10-12, 2021
                                            </p>
                                    </div>
                                    <div className="home-podcasts-item">
                                    <a target="_blank" href="https://www.datastax.com/resources/podcast/open-source-impact-in-academia">
                                                <h3>Open Source’s Impact in Academia with Open@RIT's Stephen Jacobs</h3>
                                    </a>
                                            <p>
                                            December 10th, 2020, Open Source Data Podcast
                                            </p>
                                    </div>
                                    <div className="home-podcasts-item">
                                    <a target="_blank" href="https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy5maXJlc2lkZS5mbS9pbXBhY3RmdWxvcGVuc291cmNlL3Jzcw/episode/\NmQ0YTZlYTktZmRkNC00NmU3LWI4ODYtMTRiNzg1NzVjYTBk?hl=en&ved=2ahUKEwjI1Muk39ruAhVNaM0KHQxtCpIQjrkEegQIChAI&ep=6">
                                                <h3>Stephen Jacobs on Open@RIT and the future of Open in universities</h3>
                                    </a>
                                            <p>
                                            December 7th, 2020 Impactful Open Source.
                                            </p>
                                    </div>
                                    
                                </div>
                                <div className="events">
                                    <div className="news-events-item">
                                        <h3><a target="_blank" >Open@RIT Monthly Meetings</a></h3>

                                        <p>March 10, April 14, May 12<br/>
                                            Second Wednesday of the month<br/>
                                            11am via Zoom</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            
            </div>
            
            
        </div>
    );
}
/*
<CardGrid cards={projectCards}/>
*/

/*
<div className="home-routes button">
                                <a href="mailto:openatrit@gmail.com">Contact</a>   
                            </div>
*/