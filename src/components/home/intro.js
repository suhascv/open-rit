import './Intro.scss';

import * as Icon from 'react-feather';
import { useState } from 'react';

const path = process.env.PUBLIC_URL;

export default function Intro(){
    const [sound,setSound] = useState(true);
    const [play,setPlay] = useState(true);


    function handleVolume(){
        let video = document.getElementById('intro-animation');
        video.volume = sound?0:1; 
        setSound(sound?false:true);
        
    }

    function handlePlay(){
        var video = document.getElementById('intro-animation');
        if(video.paused){
            video.play();
            setPlay(true);
        }
        else{
            video.pause();
            setPlay(false);
        }
    }
    return(
        <div className="intro"> 
                <img id="intro-animation" src={path+"/images/general/homeCover.jpg"}>

                </img>
                <div className="mobile-poster">
                    <h1>Open@RIT</h1>
                </div>
                <div className="overlay" >
               </div>
               <div className="poster" >
                <h1>Open@RIT</h1>
                <h2>The University's Open Programs Office and Key Research Center</h2>
               </div>

               {sound?<Icon.Volume2 style={{display:'none'}}className="volume-button" onClick={handleVolume} size={35}/>:<Icon.VolumeX style={{display:'none'}} className="volume-button" onClick={handleVolume} size={35}/>}
               {play?<Icon.Pause style={{display:'none'}} className="pause-button" onClick={handlePlay} size={35}/>:<Icon.Play style={{display:'none'}} className="pause-button" onClick={handlePlay} size={35}/>}

        </div>
    )
}


/*
onClick ={handlePlay}--->overlay and poster

<video id="intro-animation" alt="Avatar" class="video" loop="true" onClick={handlePlay} autoPlay>
                    <source src={"https://www.rit.edu/magic/sites/rit.edu.magic/files/videos/magic-hpm-version-color.mp4#t=1/"}>
                    </source>
                </video>
*/