import React from 'react';
import './Research.scss';
//import CardSlider from '../cardSlider.js';
import CardGrid from '../cardGrid.js'; 

const path= process.env.PUBLIC_URL;

const loeremIspum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
const projectCards=[
    {
        image:'/images/general/card.jpg',
        primary:"Project 1",
        secondary:["professor","school"],
        description:[loeremIspum]
    },
    {
        image:'/images/general/card.jpg',
        primary:"Project 2",
        secondary:["professor","school"],
        description:[loeremIspum]
    },
    {
        image:'/images/general/card.jpg',
        primary:"Project 3",
        secondary:["professor","school"],
        description:[loeremIspum]
    }
    
]

export const ResearchLanding = () =>(
    <div>
        <CardGrid cards={projectCards}/>
    </div>
    )

const Fellows=()=>(
    <div>
        <p>
            {loeremIspum}
        </p>
    </div>
)

const Grants = () =>(
    <div>
        <p>
        Does your grant proposal require your work to be Open licensed? 
         Does it require a sustainability plan?  Open Work research and projects
          that are designed, or restructured, to support a community of contributors
           can increase the impact and translation of your work. We can help you with
            licensing and creating a sustainability plan that adds Open@RIT in for
             infrastructure and community development support through our LibreCorps
              team.  Set a meeting with us by using this <a target="_blank" href="https://calendly.com/open-rit-org/">Calendly link</a>
        </p>
    </div>
);

const LibreCorps = () =>(
    <div className="libre-corps">
        <div className="img">
        <img src={path+"/images/research/libreCorps.svg"}></img>
        </div>
        <p>
        
        LibreCorps is our faculty and staff led, full-time student driven initiative that provides 
        support for Open Work on and off campus. LibreCorps has worked with several different projects
         from UNICEF Innovation, most recently under contract to mentor some of their <a>UNICEF Ventures </a>
          cohorts on  Open infrastructure, pipeline and practices with the goal of creating an environment
           coducive to building community. RIT co-ops built the first instance of <a>Fedora Badges</a>, contributed
            to <a target="_blank" href="https://openaps.org/">Open APS</a> and Night Scout and more.  If you've got a project you think can benefit from
             LibreCorps support, either by contract or as part of a grant, send an email to 
             <a href="mailto:sj@mail.rit.edu"> sj@mail.rit.edu</a> to discuss it.
        </p>
    </div>
);

const OpenWork = () =>{
    return(
    <div className="open-work">
        <p>
        The research effort that indirectly led to Open@RIT, and directly led to our recent
         Alfred P. Sloan Foundation Grant (see more in "News" above)  was 
         <a target="_blank" href="https://www.fordfoundation.org/media/5811/rit-ford-sloan-one-pager-final.pdf" 
         >Conceptual Mismatches</a>,
          a qualitative study of <a target="_blank"  
          href="https://pypi.org/">PyPI</a> that was funded as part of the first 
          <a >Critical Digital
           Infrastructure Research</a> program, supported by the Ford Foundation and the Alfred
            P. Sloan Foundation. The work was presented as part of a research panel held by
             the Ford and Sloan foundations on August 20th, 2020.
        </p>
        <section>
            <div className="open-work-img">
            <img src={path+"/images/research/alfredPSloanLogo.png"}>
            </img>
            </div >
            <div className="open-work-info">
                <h3><a target="_blank" href="https://sloan.org/">Alfred P. Sloan Foundation </a></h3>
                <p>The Alfred P. Sloan Foundation awarded Open@RIT
                 with funds to support a LibreCorps dedicated to 
                 faculty and staff efforts in Open Work that powers
                the Open@RIT Fellows program.</p>
            </div>
        </section>
        <section>
            <div className="open-work-img">
            <img src={path+"/images/research/fordFoundationLogo.jpeg"}>
            </img>
            </div >
            <div className="open-work-info">
                <h3><a target="_blank" href="https://www.fordfoundation.org/media/5811/rit-ford-sloan-one-pager-final.pdf">Conceptual Mismatches</a></h3>
                <p>Conceptual Mismatches, a qualitative 
                    study of PyPI was part of the first Ford and 
                    Alfred P. Sloan Foundation's Critical Digital Infrastructure 
                    research cohort. The study ran for the calendar year of 2019 
                    and presented as part of a research colloquium held by the 
                    foundations on August 20th, 2020.</p>
            </div>
        </section>
    </div>
    );
}

const OurOpenProjects = () =>(
    <div className="our-open-projects">
        <div className="our-open-projects-des">
            <p>
            <a target="_blank" href="https://deafworldaroundyou.org">World Around You</a> distributes "digital picture books" with multiple signed and textural 
            languages to support the literacy of Deaf children world-wide.  The development of the 
            platform was led by Dr. Christopher Kurz with  Open@RIT Director Stephen Jacobs as co-pi 
            in partnership with <a>Second Avenue Learning</a>, Inc. The development  was funded by several 
            grants from the <a>All Children Reading Grand Challenge</a> and recently won the award for  
            <a> Inclusive Education</a> from the <a>Mobiles in Education Alliance</a>
            </p>
        </div>
        
        <img  className="our-open-projects-img" src={path+"/images/research/secondavenueLogo.png"}></img>
        <img className="our-open-projects-img" src={path+"/images/research/AllChildrenReadingLogo.jpg"}></img>
        <img className="our-open-projects-img" src={path+"/images/research/mEducationAllianceLogo.png"}></img>
        
        
    </div>
)

export const ResearchSections =[LibreCorps,Grants,OpenWork,OurOpenProjects];

/*
<CardSlider Identity="slider-1" 
                                cards={projectCards} 
                                cardMargin={16} 
                                primaryFont={14}
                                sliderBackground={"#ffffff"}
                                sliderHeight={400}
                                infoHeight={40}
                                primaryColor={"#F76902"}
                                infoBackground={"#ffffff"}
                                
                                />

*/