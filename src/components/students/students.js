import React from 'react';
import './Students.scss';



const UndergraduateCourses = () =>(
    <div className="undergraduate-courses">
        <p className="ug-tag" style={{padding:0}}> 
        Open@RIT is proud to support courses these introductory courses offered by the School of Interactive Games and Media and the Department of English.
        </p>
        <div>
            <a target="_blank" href="https://www.rit.edu/study/free-and-open-source-software-and-free-culture-minor">
                IGME-582 Humanitarian Free and Open Source Software Development</a>
            <p>Open Source has the potential
                to be a great boon for humanitarian efforts. The HFOSS Development
                course is the first step to making that come true. This course
                introduces students to the tools and processes critical to Open
                Source development, and allows them to contribute to humanitarian
                software, game, and interactive media communities from the get go.
                Students will also document their projects on HFOSS community hubs,
                immersing them to the fullest extent of working in Open Source.
                <br/>
                <br/>
                
            </p>
        </div>
        <div>

            <a target="_blank" href="https://calendly.com/open-rit-org/">ENGL-450 Free & Open Source Culture</a>
            <p>A lot has changed in the relationship between authorship and cultural
                production. That’s what the Free & Open Source Culture Course
                is all about. Students will utilize factors such as commerce,
                culture, law, and technology to analyze how the cultural norms
                surrounding authorship transformed over the past three centuries, and
                use those transformations to see the roles software and the latest
                media technologies can play in facilitating Free and Open cultures.
                <br/>
                <br/>
                
            </p>
        </div>
        
    </div>
)


const AcademicMinor = () =>(
    <div className="academic-minor">
    <p style={{padding:0}}>
    For students who wish to have a
deeper understanding of open work, they’re in luck! RIT provides
formal classes and a minor in Free and Open Source Software and Free
Culture through the School of Interactive Games and Media. These
classes are open to all RIT students. <br/><br/>

The minor in Free and Open Source Software and Free Culture is the first
of its kind in the country with courses created and taught by Open @
RIT director Stephen Jacobs and Professor Amit Ray. With a
combination of computing and liberal arts courses, the minor gives
students a greater comprehension of the technologies and practical
application of open work, as well as its societal impacts. <br/><br/>

As part of the required courses they directly participate in the open
community through research, analysis, and create their own open
source projects. Not only is this a great way for students to make an
immediate splash in open work, open source communities are often
where companies search to find new employees, meaning students in the
minor have a massive advantage when looking for a job after
graduation. There’s no better way to start one’s career in open
work! <a target="_blank" href="https://www.rit.edu/computing/school-interactive-games-and-media">School 
         of Interactive Games and Media.</a>
    </p>
    </div>
);
const Foss =() =>(
        <div className="foss">
           <p style={{padding:0}}>
            <a href="https://fossrit.github.io/">FOSS@MAGIC </a>
            is RIT’s Free and Open Source Software (FOSS) student group based in the 
            RIT MAGIC Center. Students in the organization actively engage with the 
            open source community and/or start their own and show their work annually
            at Imagine RIT and Maker Faire Rochester</p> 
            
        </div>)


const CoOps = () =>(
        <div className="co-ops">
            <p style={{padding:0,marginBottom:0}}>
            LibreCorps is an initiative of Open@RIT that connects RIT students with opportunities 
            that use Open Source in a humanitarian and/or civic entity. It also supports the projects
             of the Open@RIT Fellows Program. LibreCorps employs students in Co-ops and part-time positions.
               When openings are available, they’ll be listed below
            </p>
            <h2>Some organizations students have worked with include:</h2>
            <div style={{padding:"10px 0"}}>
                <a target="_blank" href="http://www.unicefinnovationfund.org/home" style={{paddingTop:0}}>UNICEF Innovation and Innovation Fund</a>
                <a target="_blank" href="https://openaps.org">Open APS</a>
                <a target="_blank" href="https://www.nightscout.info"> Night Scout</a>
                <a target="_blank" href="https://sugarlabs.org">Sugar Labs</a>
                <a target="_blank" href="https://www.opencompute.org/">OCP</a>
            </div>
            <h2>Current Openings:</h2>
            <p>
               None at this time.
            </p>
        </div>);

const StudentsEvents = () =>(
    <div className="student-events">
        <div className="term">
            <h2>Fall 2021</h2>
            <p>Nov 15 <a>Lorem ipsum dolor</a></p>
            <p>Nov 15 <a>Lorem ipsum dolor</a></p>
        </div>
        <div className="term">
            <h2>Spring 2022</h2>
            <p>Nov 15 <a>Lorem ipsum dolor</a></p>
            <p>Nov 15 <a>Lorem ipsum dolor</a></p>
        </div>
    </div>
)

export const StudentsSections = [UndergraduateCourses,AcademicMinor,Foss,CoOps];