import './Navigation.scss';
import React, { useEffect, useState } from 'react';
import * as Icon from 'react-feather';


var path = process.env.PUBLIC_URL;
var routeDict ={}
routeDict[path+'/']='';
routeDict[path+"/students"]="Students";
routeDict[path+"/faculty"]="Faculty";
routeDict[path+"/research"]="Research";
routeDict[path+"/about"]="About";


const isMobile=()=>{
    return window.innerWidth<=750?true:false;
}


class Navigation extends React.Component{
    constructor(props){
        super(props);
        this.state={
            display:false,
            selected:'',
            bar:'',
            route:'',
            name:'',
            dropDownVisibility:'none',
            subRoutes:[],
            mobDropDown:false,
            logoAnimation:""
        };
        this.handleDisplay = this.handleDisplay.bind(this);
        this.handleVisibility = this.handleVisibility.bind(this);
        this.handleDropDown = this.handleDropDown.bind(this);
        this.handleMobileDropDown=this.handleMobileDropDown.bind(this);
        this.handleMobDropDown=this.handleMobDropDown.bind(this);
        this.handleLogo= this.handleLogo.bind(this);
        
    }

    handleLogo(){
        let tmp = this.state.logoAnimation===""?"animate__animated animate__pulse ":""
        this.setState({logoAnimation:tmp});
    }

    handleVisibility(name,id){
        
        let ele = document.getElementById('navbar');
        if(id){
        let nav = document.getElementById(id);
        let rect =nav.getBoundingClientRect();
        let dropdown = document.getElementById('dropdown-content');
        dropdown.style.right = (window.innerWidth- rect.right-10)+"px";
        }
        
        if(this.state.dropDownVisibility==='none'){
            ele.style.height="auto";
            this.setState({dropDownVisibility:'flex',name:name});
        }
        else{
            if(name===this.state.name){
            ele.style.height="76px";
            this.setState({dropDownVisibility:'none',name:''})
            }
            else{
                ele.style.height="auto";
            this.setState({dropDownVisibility:'flex',name:name})
            }
        }
        
        

    }

    
    
    handleMobDropDown(name){
        if(this.state.mobDropDown){
            if(this.state.name===name){
            this.setState({mobDropDown:false})
            }
            else{
                this.setState({name:name})
            }
        }
        else{
            this.setState({mobDropDown:true,name:name});
        }

    }

    handleMobileDropDown(){
        this.setState({dropDownVisibility:'none'});
        this.handleDisplay();
    }

    handleDropDown(data){
        this.setState({subRoutes:data});
        this.setState({name:''})
    }
    handleDisplay(){
        let display=this.state.display;
        this.setState({display:display?false:true});
        this.setState({bar:display?'':'cross'});  
    }

    componentDidMount(){
        try{
            document.title=routeDict[window.location.pathname]+" | Open@RIT";
            if(routeDict[window.location.pathname]==='Students' || routeDict[window.location.pathname]==='Faculty'){
                this.setState({selected:'Programs'});
            }
            else{
                this.setState({selected:window.location.pathname});
            }
        }
        catch(e){
            document.title="Open@RIT";
        }

    }
    

    render(){
                return(   
                    <div className="navbar" id="navbar">
                        <header className={"navigation"} >
                            <a className="logo" href={path+"/"} >
                                <img className={this.state.logoAnimation+"image"} 
                                    src={this.props.logo} 
                                    alt="logo"
                                    onMouseEnter={this.handleLogo}
                                    onMouseLeave={this.handleLogo}
                                    ></img>
                            </a>
                            <div id={'web-only-options'} className={'web-only-options'} style={{}}>
                                {this.props.navigations.map(nav=>{ 
                                    return(
                                        nav.dropdown?                                       
                                            <a className="animate__animated animate__backInDown animate__fast dropdown"
                                            style={(this.state.name===nav.name && this.state.dropDownVisibility==='flex')||(this.state.selected==='Programs')?
                                            {color:'black',borderBottom:'10px solid #F76902'}:{}}
                                            key={nav.name}
                                            id={"open-nav-id-"+nav.name}
                                            onClick ={()=>{
                                                this.handleDropDown(nav.subRoutes);
                                                this.handleVisibility(nav.name,"open-nav-id-"+nav.name);
                                            }}
                                            onMouseEnter ={()=>{
                                                this.handleDropDown(nav.subRoutes);
                                                this.handleVisibility(nav.name,"open-nav-id-"+nav.name);
                                            }}
                                            >
                                            <div className="drop-title" >
                                            {nav.name}
                                            {(this.state.name===nav.name && this.state.dropDownVisibility==='flex')?
                                            <Icon.ChevronUp className="animate__animated animate__flipInX animate__faster"/>:
                                            <Icon.ChevronDown className="animate__animated animate__flipInX animate__faster"/>}
                                            </div>        
                                        </a>:
                                        <a className="animate__animated animate__backInDown animate__fast dropdown direct-option"
                                        key={nav.name}
                                        id={"open-nav-id-"+nav.name}
                                        href={path+nav.url}
                                        style={(this.state.selected===path+nav.url)?{
                                            color:'black',
                                            borderBottom:'10px solid #F76902'
                                        }:null}
                                        onMouseEnter ={()=>{
                                            this.setState({dropDownVisibility:'none'});
                                            
                                        }}
                                        >
                                        <div className="drop-title" >
                                        {nav.name}
                                        
                                        </div>        
                                    </a>
                                        
                                        )})
                                }
                            </div>
            
                            
                            <div id="mobile-only-bar" className={"mobile-only-bar "+this.state.bar} onClick={this.handleDisplay}>
                                {this.state.display?<Icon.X  size={32} onClick={this.handleMobileDropDown}/>:<Icon.Menu size={32}/>}
                            </div>

                        </header>
                        
                        <div id="dropdown-content"className="dropdown-content animate__animated animate__fadeInDown animate__fast" 
                            style={{display:this.state.dropDownVisibility}}
                            onMouseLeave={this.handleMobileDropDown}
                            >
                            {this.state.subRoutes.map((sb,index)=>(
                                <div key={index} className="dropdown-section">
                                    <a href={path+sb.url}>
                                        {sb.header}
                                    </a>
                                </div>
                            ))}    
                        </div> 

                        <div id="mobile-only-options" className="mobile-only-options animate__animated animate__fadeInRight" 
                             style={{display:this.state.display?'flex':'none'}}>
                            {
                                this.props.navigations.map(nav=> {     
                             return nav.dropdown?(             
                                        <div className="animate__animated animate__fadeInRight animate__fast dropdown"
                                            key={nav.name}>
                                            <div className="drop-title" onClick ={()=>{
                                                this.handleDropDown(nav.subRoutes);
                                                this.handleMobDropDown(nav.name);
                                                }
                                            }>
                                            <h3>{nav.name}</h3>
                                            {(this.state.mobDropDown && nav.name===this.state.name)?
                                                <Icon.ChevronUp className="chevron"/>:
                                                <Icon.ChevronDown className="chevron"/>
                                            } 
                                        </div>    
                                 
                                        {(this.state.mobDropDown && nav.name===this.state.name)?<MobDropDown routes={nav.subRoutes}/>:null}  
                                    </div>):
                            (<a className="mobile-direct-option animate__animated animate__fadeInRight animate__fast"
                            href={path+nav.url}
                            key={nav.name}>
                            <h3>{nav.name}</h3>   
                            </a>)
                                
                        })}
                    
                    <a href="mailto:openatrit@gmail.com"className="contact-mobile"> 
                        <h2>Contact</h2> 
                    </a>
                           
                    </div>
                    
            
                    </div>
                );
            } 
}

export default Navigation;


const MobDropDown=(props)=>(

     <div className="sub-route-1">
         {props.routes.map((subroute,index)=>(   
        <a key={index} 
            className="sub-route-1-header animate__animated animate__fadeInRight" 
            href={path+subroute.url}>
            {subroute.header}</a>  
         ))}
     </div>   
    )
         



