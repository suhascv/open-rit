import React from 'react';
import ReactTooltip from 'react-tooltip';
import'./Calendar.scss';
//import calendarEvents from './calendarEvents.json';
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid';
import * as Icon from 'react-feather';
import ICalendarLink from "react-icalendar-link";



async function getEvents(){
    let resp = await fetch('https://open-rit-admin.herokuapp.com/api/events');
    let jsonResp  = resp.json();
    return jsonResp;
}


//convert datetime to AM/PM
function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + '' + ampm;
    return strTime;
  }

//calculate duration of two date-time-object
  function diff_minutes(dt2, dt1) 
  {
   var diff =(dt2.getTime() - dt1.getTime()) / 1000;
   diff /= 60;
   return Math.abs(Math.round(diff));
   
  }



//unique event ids;
var ids = 0;


export default class OpenCalendar extends React.Component{
constructor(props){
    super(props);
    this.state={
        display:'none',
        event: [],   
        extended:false,
        calendarEvents:""
    }
    this.handleEvent=this.handleEvent.bind(this);
    this.handleDisplay=this.handleDisplay.bind(this);
}


//controls the display of event card
handleDisplay(basic,extended){

    if(basic){
    
    this.setState({event:basic});
    
    }
    if(extended){
        this.setState({extended:extended});
        console.log(this.state.extended);
    }
    else{
        this.setState({extended:false})
    }
    var ele = document.getElementById('mobile-tip');
    ele.style.display= ele.style.display==='none'?'flex':'none';
    
}


//handling event content
handleEvent(info){
    
    let start = false;
    let end =false;

    let allInfo = [{"tag":"Title","text":info.event.title},] 
    let eID = ids+"-tool";
    ids++;
    let eventObject ={'title':info.event.title}
    

    if(info.event.start){
        let displayDate = info.event.start;
        let displayStart = displayDate.getMonth()+1+"/"+displayDate.getDate()+"/"+displayDate.getFullYear();
        start=formatAMPM(info.event.start);
        allInfo.push({"tag":"start","text":""+displayStart+" "+start});
        eventObject['startTime']=info.event.start;
        
        
        //console.log(displayDate)
    }
    if(info.event.end){
        
        let displayDate = info.event.end;
        let displayEnd = displayDate.getMonth()+1+"/"+displayDate.getDate()+"/"+displayDate.getFullYear();
        end=formatAMPM(info.event.end);
        allInfo.push({"tag":"end","text":""+displayEnd+" "+end});
        eventObject['endTime']=info.event.end;
        let duration = diff_minutes(info.event.start,info.event.end);
        eventObject['duration']=duration
        allInfo.push({'tag':'duration','text':duration});
    }

    let eventColor = "purple";
    
    if(info.event.extendedProps){
        var otherInfo = info.event.extendedProps;
        for(let i in otherInfo){
            if(i==="marker"){
                eventColor=otherInfo[i];
            }
            else{
                if(i==="location"){
                    
                    eventObject['location']=otherInfo[i]
                }
                else if(i==="description"){
                    eventObject['description']=otherInfo[i]
                
                }
            allInfo.push({"tag":i,"text":otherInfo[i]});
            }
        }
    }

    let tick=Object.keys(eventObject).length===6?eventObject:null;
    return(
   


    <div className="event-tool" data-tip data-for={eID} style={{opacity:1,backgroundColor:eventColor}} onClick={()=>this.handleDisplay(allInfo,tick)}>
        {info.event.title}
       
        <ReactTooltip id={eID} className="tool-tip" place="top" effect="float">
            {info.event.title}
        </ReactTooltip>
    
    
    </div>
    

    )
}

async componentDidMount(){
    ids=0;
    let resp = await getEvents();
    this.setState({calendarEvents:resp});
    console.log(this.state.calendarEvents);
}


render(){
    
    return (
    <div className="container">
        <div className="calendar">
        <FullCalendar
            plugins={[ dayGridPlugin ]}
            initialView="dayGridMonth"
            weekends={true}
            events={this.state.calendarEvents}
            eventContent ={this.handleEvent}
            eventBackgroundColor="green"
            />
        
        <div id="mobile-tip" className="mobile-tip" style={{display:this.state.display}}>
        <Icon.X className="close" size={'2rem'}onClick={()=>this.handleDisplay(null,null)}/>
        <div className="event-info">
        {this.state.event.map((info,index)=>(<DecorateEvent key ={index} tag={info.tag} text={info.text}/>))}
        </div>   
        {this.state.extended?<ICalendarLink className="download-calendar" event={this.state.extended}>Add to Calendar </ICalendarLink>:null}
            </div>
            
        </div>
    </div>
    );
}

}



const DecorateEvent=(props)=>{
    //console.log(props)
    if(props.tag==='location'){
        return(
            <div className="tip-title">
                <span className="info-tag">{"location"}<Icon.MapPin /></span>
                <span className="info-text">{props.text}</span>
                
            </div>
        )
    }
    else if(props.tag==='link'){
        return(
            <div className="tip-title">
                <span className="info-tag">
                {"event link"}<Icon.Link />
                </span>
                <span className="info-text">
                <a href={props.text}>
                {"click here"}
                </a>
                </span>
            </div>
        )
    }
    else if(props.tag==='Title'){
        return(
            <div className="tip-title">
                {props.text}
            </div>
        )
    }
    else if(props.tag==='duration'){
        return(
            <div className="tip-title">
                <span className="info-tag">{"duration"}<Icon.Watch /></span>
                <span className="info-text">{props.text+" min"}</span>
            </div>
        )
    }
    
    else if(props.tag==='add-to-calendar'){
        return(
        <button>
        add to calendar
        </button>
        )
        

    }
    else{
        return(
            <div className="tip-title">
                <span className="info-tag">{props.tag}</span>
                <span className="info-text">{props.text}</span>
            </div>
        )
    }
}