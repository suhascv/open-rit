import React from 'react';
import './Faculty.scss';

const OpenFellows = () =>(
    <div>
        <p>Open@RIT has received approximately $500,000 in funding from the Alfred P. 
            Sloan Foundation, a supporter of science, technology, engineering, math, 
            and economics, to assist in the goal of expanding the use of Open Work and 
            Open communities. Such goals are accomplished via our Fellows Program.
        <p/>
        <p>
            The Open@RIT Fellows program is dedicated to assisting RIT faculty and 
            staff in the development and maintenance of their research and projects 
            involving Open Work, providing consultation services, and building 
            communities around their projects.
            </p>
        <p/>
            Our first Call for Proposals has ended for the near 
            future, but don’t fret! When Fall Semester 2021 comes 
            around, we’ll be releasing another CFP, so keep those 
            Open Work projects warm and ready!
        </p>
    </div>
)


const ProfessionalDevelopment = () =>(
    <div >
        <p >
        Want to get started on developing your Open Work project or 
        research? You’ve come to the right place! Here you’ll find the 
        resources from Open@RIT, as well as other Open Work communities to 
        help you build and maintain a project that supports the ideals and 
        goals of Open Work. We'll be publishing a collection of resources 
        this summer and plan to offer some workshops/seminars in fall of 
        2021.
        </p>
    </div>
);

const OfficeHours = () =>(
    <div className="office-hours">
        <p>
        Have a current project you want help with, or a new one you want to start? 
        We're happy to provide advice and/or answer questions you may have about Open 
        Work. Just use the link below to set up a time to talk.
        </p>
        <a target="_blank" href="https://calendly.com/open-rit-org/">Calendly Link</a>
    </div>
);



export const FacultySections = [OpenFellows,ProfessionalDevelopment,OfficeHours];