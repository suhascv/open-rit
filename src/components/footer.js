import './Footer.scss';

import {ReactComponent as ReactLogo} from '../images/footerBlack.svg';
import * as Icon from 'react-feather';
import {ReactComponent as TiktokLogo}  from '../images/tiktok.svg';
var path = process.env.PUBLIC_URL;

export default function Footer(){


    return(
        <footer>
            <a href={path+"/"}>
                <ReactLogo className="icon"/>
            </a>
            <a target="_blank" href="https://goo.gl/maps/JCTt9bpsmRSgF1KX7">
                <p>
                    One Lomb Memorial Drive,<br/>
                    Rochester, NY 14623
                </p> 
            </a>
            <div className="social">
                <a target="_blank" href="https://twitter.com/openatrit" className="social-icon"><Icon.Twitter/></a>
                <a className="social-icon"><Icon.Linkedin/></a>
                <a target="_blank" href="https://gitlab.com/open-rit" className="social-icon"><Icon.Gitlab/></a>
                <a target="_blank" href="https://instagram.com/openatrit" className="social-icon"><Icon.Instagram/></a>
                <a target="_blank" href="https://tiktok.com/@openatrit" className="social-icon"><TiktokLogo className="default-footer-fill"/></a>    
            </div>
            <a href="mailto:openatrit@gmail.com">
                <button>
                    Contact
                </button>
            </a>
            
                
        </footer>
    )
}