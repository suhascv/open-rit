import "./CardSlider.scss";
import React from "react";
import * as Icon from "react-feather";

function isMobile(){
  return window.innerWidth<750?true:false;
}

export default class CardSlider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      card: this.props.cards,
      sliderHeight: this.props.sliderHeight ? this.props.sliderHeight : 300,
      cardWidth: this.props.displayNumber?80/this.props.displayNumber:21,
      cardMargin: "16px",
      sliderBackground: this.props.sliderBackground?this.props.sliderBackground:"white",
      start: 0,
      end: 4,
      numberOfCards: this.props.cards.length,
      animation: "",
      displayNumber:this.props.displayNumber?this.props.displayNumber:4,
      displayArrow:this.props.cards.length<=this.props.displayNumber?false:true

    };
    this.handleArrows = this.handleArrows.bind(this);
    this.handleCards = this.handleCards.bind(this);
  }

  

  handleArrows() {
    this.setState({
      ArrowVisibility: this.state.ArrowVisibility ? false : true
    });
  }

  handleCards(direction) {
    if (direction === "right") {
      this.setState({
        start: this.state.start + this.state.displayNumber,
        animation: "animate__animated  animate__slideInLeft fast"
      });
    } else {
      this.setState({
        start: this.state.start - this.state.displayNumber,
        animation: "animate__animated  animate__slideInRight fast"
      });
    }
  }

  render() {
    
    const cardsSliders = [];
    if(this.state.displayArrow){
    cardsSliders.push(
      <span className="slider-arrow" onClick={() => this.handleCards("left")}>
        <Icon.ChevronLeft size={48}  />
      </span>
    );
    }

    let j = this.state.numberOfCards;
    let mobile = isMobile();
    let end=mobile?j:this.state.displayNumber;
    let start =mobile?0:this.state.start;
    for (let i = start; i < start + end; i++) {

      var k;
      if(mobile){
        k=i
      }
      else{
        if (i % j < 0) {
          k = j + (i % j);
          
        } else {
          k = i % j;
        }
        
      }
      cardsSliders.push(
        <a
          className={"card " + this.state.animation+" slideOutRight"}
          target="_blank"
          href={this.props.cards[k].url?this.props.cards[k].url:null}
          key={i}
          style={
            this.props.secondary
              ? {
                  backgroundImage: `url(${this.props.cards[k].image})`,
                  backgroundSize: "100% 100%",
                  width:this.state.cardWidth+"%"
                }
              : {
                width:this.state.cardWidth+"%"
              }
          }
        >
          <div className="image" 
              style={isMobile()?{height:'200px'}:{height:100-this.props.infoHeight+"%"}}
          >
            {this.props.secondary ? null : (
              <img src={this.props.cards[k].image}></img>
            )}
          </div>
          <div
            className="info"
            style={{
              background: this.props.secondary ? "transparent" : this.props.infoBackground,
              color: this.props.secondaryColor,
              Height: isMobile()?'auto':this.props.infoHeight + "%"
            }}
          >
            <h2 style={{ color: this.props.primaryColor}}>
              {this.props.cards[k].primary}
            </h2>
            {this.props.cards[k].secondary.map((sec, index) => (
              <h4  key={index}>{sec}</h4>
            ))}
            {this.props.cards[k].description.map((des, index) => (
              <p  key={index}>{des}</p>
            ))}
          </div>
        </a>
      );
    }
    if(this.state.displayArrow){
    cardsSliders.push(
      <span className="slider-arrow" onClick={() => this.handleCards("right")}>
        <Icon.ChevronRight size={48} />
      </span>
    );
    }
    return (
      <div
        className="Slider"
        id={this.state.id}
        style={{
          minHeight: this.props.sliderHeight,
          background: this.state.sliderBackground,
          width:"100%",
        }}
        onMouseEnter={this.handleArrows}
        onMouseLeave={this.handleArrows}
      >
        {cardsSliders.map(card => card)}
      </div>
    );
  }
  
}
