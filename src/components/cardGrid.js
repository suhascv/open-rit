import ProjectImage from '../images/sampleProject.jpg';
import './CardGrid.scss';

function isMobile(){
    return window.innerWidth<750?true:false;
  }

export default function CardGrid(props){
    var Columns= props.Columns?(90/props.Columns)+"%":30+"%";
    Columns=isMobile()?"100%":Columns;

    const ImageHeight = props.ImageHeight?props.ImageHeight+'px':'210px';
    const ImageWidth = props.ImageWidth?props.ImageWidth+"%":'100%';
    return(
        <div className="cards-fellows">
            {props.cards?props.cards.map((card,index)=>(
                <a key = {index} className="cards-fellow-card"
                    style={{width:Columns}}
                >
                <div className="cards-fellow-image" style={{height:ImageHeight,width:ImageWidth}}>
                    <img src={card.image}></img>
                </div>
                <div className="cards-fellow-description">
                    
                    <h3 >{card.primary}</h3> 
                    
                    {card.secondary?card.secondary.map((sec,ix2)=>(
                       <h4 key={ix2}>{sec}</h4> 
                    )):null}
                    {card.description?card.description.map((des,ix3)=>(
                       <p key={ix3}>{des}</p> 
                    )):null}
                </div>
            </a>
            )):
                <a className="cards-fellow-card">
                    <div className="cards-fellow-image">
                        <img src={ProjectImage}></img>
                    </div>
                    <div className="cards-fellow-description">
                        <h3>We Must Tear Down the Barriers That Impede Scientific Progress</h3>
                    </div>
                </a>
        }                                     
        </div>
    )
};

